<?php
/**
 * Template Name: Home Page
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container" class="one-column">
				<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
	
			<div id="content" role="main">
			



				<h1 class="one-column-title" id="videos">Featuring</h1>
				
				<div id="featured-videos">
				<?php  // find out how many video posts actually have videos
				$numbvids = 0;
				$numberofvideos = new WP_Query('cat=104&posts_per_page=25'); 
				if ( $numberofvideos->have_posts() ) : while ( $numberofvideos->have_posts() ) : $numberofvideos->the_post();
					  $youtubelinks = get_post_custom_values('youtube',$post->ID);
						    if ($youtubelinks != NULL){  
				  			foreach ( $youtubelinks as $key => $value ) {
				  				$numbvids++;
				  				}
				  			}
				endwhile; else:
				endif; 		  
				  			
				$videocount = 0;
				$videoquery = new WP_Query('cat=104&posts_per_page=25'); 
				
					if ( $videoquery->have_posts() ) : while ( $videoquery->have_posts() ) : $videoquery->the_post(); 
				       		// pull some info from youtube
						    $youtubelinks = get_post_custom_values('youtube',$post->ID);
						    if ($youtubelinks != NULL){  
				  			foreach ( $youtubelinks as $key => $value ) {
				  				$videocount++;
								$vid = stripslashes($value);  
								$string = $vid;  
								$url = parse_url($string);  
								parse_str($url['query']);  
								// $feedJSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id={$v}&key=AIzaSyAovV1yEe7rNXZ_Nov-6qHBRQis3xktl7U&part=contentDetails");  
								// $feed = json_decode($feedJSON);
								// echo "<pre>".print_r($feed,1)."</pre>";
								// $video = parseVideoEntry($entry);  
								// $video_info['thumbnail'] = stripslashes($video->thumbnailURL);  
								$video_info['thumbnail'] = 'https://i3.ytimg.com/vi/'.$v.'/hqdefault.jpg';
								/* EMBED CODE <iframe width="425" height="349" src="https://www.youtube.com/embed/<?php echo $v; ?>" frameborder="0" allowfullscreen></iframe> */
								
								echo '<div id="video-'.$videocount.'" style="position:relative; display:';
								echo ($videocount == 1) ? "block;top:0px; left:0px;":"none; top:0px; left:911px;";
								echo '">';
								echo '<div class="videothumb"><a href="'.get_permalink().'"><img src="'.$video_info['thumbnail'].'"></a></div>';
								echo '<div class="vidselect"><ul>';
								for ($i=1; $i <= $numbvids; $i++) 
									{ echo ($videocount == $i) ? "<li class='current'><a href=\"#videos\">&#x2713;</a></li>" : "<li><a href=\"#videos\" onclick=\"vidflip('".$videocount."','".$i."');\">&#x2713;</a></li>"; }
								echo '</ul></div><div class="vidtitle">';
								the_title();
								echo '</div><div class="vidinfo">';
								$vidinfo = get_post_custom_values('video-description',$post->ID); 
								if ($vidinfo != NULL) {  foreach ( $vidinfo as $key => $value ) { echo $value;} };
								echo '</div>';
								echo '<div class="videolink"><a href="'.get_permalink().'">Watch this video &gt;</a></div></div>';
								}
							}
						?>
				<?php endwhile; else: ?>
				<?php endif; ?>
				<div style="clear:both"></div>
				<script type="text/javascript"> // trigger the start of video rotation
				var videostart;
				videostart = 1;
				var t;
				
				t=setTimeout("vidflipauto(videostart,'<?php echo $numbvids; ?>')",8000);
				$(".vidselect ul li a").click(function(){clearTimeout(t);});
				
				function vidflipauto(curvid, totvid) {
					if (curvid == totvid) { curvid = 1; vidflip(totvid, curvid);}
					else {curvid++; vidflip(curvid-1, curvid);}
				}

				function vidflip(current, next) {
				 	var $lefty = $("#video-"+current);
				 	$lefty.show();
					$lefty.animate({ left: -911},250,function(){
						$lefty.hide(); 
						$lefty.css("left","911px");
						var $righty = $("#video-"+next);
						$righty.show();
						$righty.animate({ left: 0},250);	
	 					t=setTimeout(function(){vidflipauto(next,'<?php echo $numbvids; ?>')},8000);
	 					videostart = next;				
						});
				}
				</script>
				</div> <!-- done with videos -->
				
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div id="bar">
					<div class="linkgraphics">
						<a href="/rap-guide-to-evolution-dvd"><img src="/images/rg-home-findoutmore.jpg" alt="find out more"><br />
						<h1>DVD OUT NOW!</h1></a>
						<div><?php $infographics = get_post_custom_values('findoutmore',$post->ID); if($infographics != NULL) { foreach ( $infographics as $key => $value ) {echo $value;};}; ?></div>
					</div>
					<div class="linkgraphics">
						<a href="/videos"><img src="/images/rg-home-watchvideos.jpg" alt="watch videos"><br />
						<h1>WATCH VIDEOS</h1></a>
						<div><?php $infographics = get_post_custom_values('watchvideos',$post->ID); if($infographics != NULL) { foreach ( $infographics as $key => $value ) {echo $value;};}; ?></div>
					</div>
					<div class="linkgraphics" style="padding:0px;">
						<a href="/discussion"><img src="/images/rg-home-shareyourideas.jpg" alt="share your ideas"><br />
						<h1>SHARE YOUR IDEAS</h1></a>
						<div><?php $infographics = get_post_custom_values('shareyourideas',$post->ID); if($infographics != NULL) { foreach ( $infographics as $key => $value ) {echo $value;};}; ?></div>
					</div>
				</div><!-- done with infographics -->
				<?php endwhile; else: ?>
				<?php endif; ?> 
				<div style="clear:both"></div>
				<h2 class="one-column-title" id="videos">Latest Blog Posts</h2>
				<?php
				$blogquery = new WP_Query('posts_per_page=5&cat=109'); 				
				while ( $blogquery->have_posts() ) : $blogquery->the_post(); ?>
				
					<?php if (in_category('109')){?>
						<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
							<div class="entry-meta">
								<?php twentyten_posted_on(); ?>
							</div><!-- .entry-meta -->
							<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>

							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div><!-- .entry-summary -->
					
				
							<div class="entry-utility">
								<span><a href="<?php echo get_permalink()?>#comments"><?php comments_number('No Comments Yet','1 Comment','% Comments'); ?>.</a></span>
								<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyten' ), __( '1 Comment', 'twentyten' ), __( '% Comments', 'twentyten' ) ); ?></span> <br />
								<?php if ( count( get_the_category() ) ) : ?>
								<?php endif; ?>
								<?php
									$tags_list = get_the_tag_list( '', ', ' );
									if ( $tags_list ):
								?>
									<span class="tag-links">
										<?php printf( __( '<span class="%1$s">Keywords: </span> %2$s', 'twentyten' ), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list ); ?>
									</span>
								<?php endif; ?>
							</div><!-- .entry-utility -->
							
						</div><!-- #post-## -->
					<?php } // end category filter for blog posts only?>
				
				
				<?php endwhile; // End the loop. Whew. ?>
				<div style="clear:both"></div>
				
				
				<h2 class="one-column-title" id="videos">Mailing List Signup</h2>
				<?php mailchimpSF_signup_form(); ?>
					
				<h2 class="one-column-title" style="margin-top: 25px;" id="videos">Recent Comments</h2>
				<?php 
				list_most_recent_comments( 'comment_format=2' );
				 ?>
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>


