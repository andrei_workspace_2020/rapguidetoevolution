<?php
/**
 * Template Name: About Page
 *
 * A custom page template with sidebars.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php
	if ( function_exists( 'wpcf7_enqueue_scripts' ) ) {
		wpcf7_enqueue_scripts();
		wpcf7_enqueue_styles();
	}
?>
		<div id="container">
						<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
			<div id="content" role="main">
			
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1 class="two-column-title-left">About the Rap Guide</h1>
				<?php the_content(); ?>
				<?php endwhile; else: ?>
				<?php endif; ?>

				<div style="clear:both"></div>
								
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>


