<?php
/**
 * Template Name: Discussion Page
 *
 * A custom page template with sidebars.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
			<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
			<div id="content" role="main">
			
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1 class="two-column-title-left">Discussion</h1>
				<?php the_content(); ?>

				<h2 class="two-column-title-left">Videos</h2>
				<?php $feed = get_post_custom_values('video-feed-description',$post->ID);
					if ($feed != NULL){  foreach ( $feed as $key => $value ) { echo $value; }; }?>
				<?php endwhile; else: ?>
				<?php endif; ?> 	
				
					<ul class="discussion-list">
						<?php // show all posts in videos
							$queryvid = new WP_Query('posts_per_page=20&cat=104'); 
							if ( $queryvid->have_posts() ) : while ( $queryvid->have_posts() ) : $queryvid->the_post(); ?>
									<li class="discusslink">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> - 
									<a href="<?php echo get_permalink()?>#comments" class="commentlink"><?php comments_number('No Comments Yet','1 Comment','% Comments'); ?>.</a>
									</li>
									
							<?php endwhile; else:
							endif;
						 ?>
					</ul>
					
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h2 class="two-column-title-left">News Feed</h2>
				<?php $feed = get_post_custom_values('news-feed-description',$post->ID);
					if ($feed != NULL){  foreach ( $feed as $key => $value ) { echo $value; }; }?>	
				<?php endwhile; else: ?>
				<?php endif; ?> 				
				
					<ul class="discussion-list">
						<?php // show all posts in blog
							$queryblog = new WP_Query('posts_per_page=20&cat=109'); 
							if ( $queryblog->have_posts() ) : while ( $queryblog->have_posts() ) : $queryblog->the_post(); ?>
									<li class="discusslink">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a> - 
									<a href="<?php the_permalink()?>#comments" class="commentlink"><?php comments_number('No Comments Yet','1 Comment','% Comments'); ?>.</a>
									</li>
							<?php endwhile; else:
							endif;
						?>
					</ul>	
				


				<div style="clear:both"></div>
								
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>


