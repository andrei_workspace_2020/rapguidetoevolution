<?php
/**
 * Template Name: Guides Page
 *
 * A custom page template with sidebars.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
						<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
				
			<div id="content" role="main">

					<h1 class="two-column-title-left">Check out some of the other rap guides</h1>
					<?php the_content(); ?>	

					<div class="guidethumbsmall">
						<a href="http://bababrinkman.bandcamp.com/album/the-rap-guide-to-human-nature" target="_blank"><img src="/images/guides-rghn.jpg"></a>
					</div>
					<div class="vidright">
						<div class="vidtitlesmall">
							<a href="http://bababrinkman.bandcamp.com/album/the-rap-guide-to-human-nature" target="_blank">The Rap Guide to Human Nature</a>
						</div>
						<div class="vidinfosmall">
							<?php
							$guideinfo = get_post_custom_values('human-nature',$post->ID); 
							if ($guideinfo != NULL) {  foreach ( $guideinfo as $key => $value ) { echo $value;} };
							?>
						</div><!-- vidinfosmall -->
					</div><!-- vidright -->
					<div class="videosbar"></div>

					<div class="guidethumbsmall">
						<a href="http://bababrinkman.bandcamp.com/album/rapconteur" target="_blank"><img src="/images/guides-rc.jpg"></a>
					</div>
					<div class="vidright">
						<div class="vidtitlesmall">
							<a href="http://bababrinkman.bandcamp.com/album/rapconteur" target="_blank">Rapconteur</a>
						</div>
						<div class="vidinfosmall">
							<?php
							$guideinfo = get_post_custom_values('rapconteur',$post->ID); 
							if ($guideinfo != NULL) {  foreach ( $guideinfo as $key => $value ) { echo $value;} };
							?>
						</div><!-- vidinfosmall -->
					</div><!-- vidright -->
					<div class="videosbar"></div>
					
					<div class="guidethumbsmall">
						<a href="http://bababrinkman.bandcamp.com/album/the-rap-canterbury-tales" target="_blank"><img src="/images/guides-rct.jpg"></a>
					</div>
					<div class="vidright">
						<div class="vidtitlesmall">
							<a href="http://bababrinkman.bandcamp.com/album/the-rap-canterbury-tales" target="_blank">The Rap Canterbury Tales</a>
						</div>
						<div class="vidinfosmall">
							<?php
							$guideinfo = get_post_custom_values('canterbury-tales',$post->ID); 
							if ($guideinfo != NULL) {  foreach ( $guideinfo as $key => $value ) { echo $value;} };
							?>
						</div><!-- vidinfosmall -->
					</div><!-- vidright -->
										
					<div class="videosbar"></div>
										<div class="guidethumbsmall">
						<a href="http://www.youtube.com/watch?v=XfpMF4S-Luw" target="_blank"><img src="/images/guides-adaptive.jpg"></a>
					</div>
					<div class="vidright">
						<div class="vidtitlesmall">
							<a href="http://www.youtube.com/watch?v=XfpMF4S-Luw" target="_blank">Adaptive Medicine</a>
						</div>
						<div class="vidinfosmall">
							<?php
							$guideinfo = get_post_custom_values('adaptive-medicine',$post->ID); 
							if ($guideinfo != NULL) {  foreach ( $guideinfo as $key => $value ) { echo $value;} };
							?>
						</div><!-- vidinfosmall -->
					</div><!-- vidright -->

					<div class="videosbar"></div>

				<div style="clear:both"></div>
								
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>


