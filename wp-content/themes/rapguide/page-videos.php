<?php
/**
 * Template Name: Videos Page
 *
 * A custom page template with sidebars.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
						<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
			<div id="content" role="main">

				<h1 class="two-column-title-left">Music Videos</h1>

				<div id="videos">	  
				 <?php			
				$videoquery = new WP_Query('cat=104&posts_per_page=0'); 
					if ( $videoquery->have_posts() ) : while ( $videoquery->have_posts() ) : $videoquery->the_post(); 
				       		// pull some info from youtube
						    $youtubelinks = get_post_custom_values('youtube',$post->ID);
						    if ($youtubelinks != NULL){  
					  			foreach ( $youtubelinks as $key => $value ) {
									$vid = stripslashes($value);  
									$string = $vid;  
									$url = parse_url($string);  
									parse_str($url['query']);  

									$feedJSON = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id={$v}&key=AIzaSyAovV1yEe7rNXZ_Nov-6qHBRQis3xktl7U&part=contentDetails");  
									$feed = json_decode($feedJSON);
									$duration = ltrim($feed->items[0]->contentDetails->duration, 'PT');
									$video_info['thumbnail'] = 'http://i3.ytimg.com/vi/'.$v.'/hqdefault.jpg';
									$mins = explode('M', $duration);
									$secs = rtrim($mins[1], 'S');
									$mins = $mins[0];
		        			?>
									<div class="videothumbsmall">
										<a href="<?php echo get_permalink(); ?>"><img src="<?php echo $video_info['thumbnail'] ?>"></a>
										<div class="videolength">Running Time <?php echo $mins.':'.$secs ?></div>
									</div>
									<div class="vidright">
										<div class="vidtitlesmall">
											<a href="<?php echo get_permalink()?>"><?php echo get_the_title()?></a>
										</div>
										<div class="vidinfosmall">
											<?php
											$vidinfo = get_post_custom_values('video-description',$post->ID); 
											if ($vidinfo != NULL) {  foreach ( $vidinfo as $key => $value ) { echo $value;} };
											?>
											<p><a href="<?php echo get_permalink()?>#comments"><?php comments_number('No Comments Yet','1 Comment','% Comments'); ?>.</a></p>
											<div class="entry-utility">
												<?php twentyten_posted_in(); ?>
												<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
											</div><!-- .entry-utility -->
										</div><!-- vidinfosmall -->
									</div><!-- vidright -->
									<div class="videosbar"></div>
									<?php
									}
							}
							else {
								$soundcloudlinks = get_post_custom_values('soundcloud-link',$post->ID);
								if($soundcloudlinks != NULL){
									foreach ( $soundcloudlinks as $key => $value ) {
									?>
									<div class="videomissing" onclick="location.href='<?php get_permalink();?>'">
										<div class="vidsoon">Video Coming Soon</div>
									</div>
									<div class="vidright">
										<div class="vidtitlesmall">
											<a href="<?php echo get_permalink()?>"><?php echo get_the_title()?></a>
										</div>
										<div class="vidinfosmall">
											<?php
											$vidinfo = get_post_custom_values('video-description',$post->ID); 
											if ($vidinfo != NULL) {  foreach ( $vidinfo as $key => $value ) { echo $value;} };
											?>
											<p><a href="<?php echo get_permalink()?>#comments"><?php comments_number('No Comments Yet','1 Comment','% Comments'); ?>.</a></p>
											<div class="entry-utility">
												<?php twentyten_posted_in(); ?>
												<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
											</div><!-- .entry-utility -->
										</div><!-- vidinfosmall -->
									</div><!-- vidright -->
									<div class="videosbar"></div>
									<?php
								
									}
								}
									
							}
							
						?>
				<?php endwhile; else: ?>
				<?php endif; ?>

				</div> <!-- done with videos -->
				

				<div style="clear:both"></div>
								
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>


