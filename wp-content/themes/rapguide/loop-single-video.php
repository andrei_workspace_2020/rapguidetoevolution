<?php
/**
 * The loop that displays a single post in the video category
 *
 * The loop displays the posts and the post content.  See
 * http://codex.wordpress.org/The_Loop to understand it and
 * http://codex.wordpress.org/Template_Tags to understand
 * the tags used in it.
 *
 * This can be overridden in child themes with loop-single.php.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.2
 */
?>

		<div id="container" class="one-column">
					
				<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
			<div id="content" role="main">


				
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				
								<div id="post-<?php the_ID(); ?>" class="post type-post status-publish format-standard category-video">
									<h1 class="one-column-title" id="videos">&ldquo;<?php the_title(); ?>&rdquo;</h1>
									<?php 
									$youtubelinks = get_post_custom_values('youtube',$post->ID);
								    if ($youtubelinks != NULL){  
						  			foreach ( $youtubelinks as $key => $value ) {
										$vid = stripslashes($value);  
										$string = $vid;  
										$url = parse_url($string);  
										parse_str($url['query']);  
/*
										$feedURL = 'http://gdata.youtube.com/feeds/api/videos/'. $v;  
										$entry = simplexml_load_file($feedURL);  
										$video = parseVideoEntry($entry);  
										$video_info['thumbnail'] = stripslashes($video->thumbnailURL);  
*/
										$video_info['thumbnail'] = 'http://i3.ytimg.com/vi/'.$v.'/hqdefault.jpg';
										}
									?>
									<div id="video-single">
									<iframe width="542" height="329" src="https://www.youtube.com/embed/<?php echo $v; ?>" frameborder="0" allowfullscreen></iframe>
								<?php } else { ?>
										<div class="videomissingwide">
										<div class="vidsoonwide">Video Coming Soon</div>
										<?php 	
										$soundcloudlink = get_post_custom_values('soundcloud-link',$post->ID);
								    	if ($soundcloudlink != NULL){  
						  					foreach ( $soundcloudlink as $key => $value ) {?>
						  					<center><object height="81" width="502"> <param name="movie" value="http://player.soundcloud.com/player.swf?url=<?php echo $value; ?>&amp;show_comments=false&amp;auto_play=false&amp;color=336600"></param> <param name="allowscriptaccess" value="always"></param> <embed allowscriptaccess="always" height="81" width="502" src="http://player.soundcloud.com/player.swf?url=<?php echo $value; ?>&amp;show_comments=false&amp;auto_play=false&amp;color=336600" type="application/x-shockwave-flash" width="100%"></embed> </object></center> <?php
											}							
										} 
									 }
								 ?></div>
								
									</div>
									</div>
									<div id="video-single-description">
										<h1>&ldquo;<?php the_title(); ?>&rdquo;</h1>
										<?php 
										$videodescriptions = get_post_custom_values('video-description',$post->ID);
									    if ($videodescriptions != NULL){  
							  			foreach ( $videodescriptions as $key => $value ) {
							  					echo $value;
							  				}
							  			} ?>
							  			<p><a href="#comments"><?php comments_number('No Comments Yet','1 Comment','% Comments'); ?>.</a></p>
							  				
							  			<div class="entry-utility">
											<?php twentyten_posted_in(); ?>
											<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="edit-link">', '</span>' ); ?>
										</div><!-- .entry-utility -->
										
							  			<?php do_action('addthis_widget', get_permalink(), the_title('', '', false),"fb_tw_sc"); ?>
									</div>
									<div style="clear:both; height: 40px;"></div>
									<div id="lyrics"></div><div id="comments"></div><div id="contributors"></div>
									<div id="lyricomm">
										<ul>
											<li id="lyricsbutton"><a href="#lyrics" onclick='onTabChanged("lyrics")'>Lyrics</a></li>
											<li id="commentsbutton"><a href="#comments" onclick='onTabChanged("comments")'>Comments</a></li>
											<li id="contributorsbutton"><a href="#contributors" onclick='onTabChanged("contributors")'>Contributors</a></li>
										</ul>
									</div>
									<div id="lyrics-box" style="display:none;">
										<div class="entry-content">
											<?php the_content(); ?>
											<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
										</div><!-- .entry-content -->
									</div>
									<div id="comments-box" style="display:none;">
									<?php echo comments_template('',false); ?>
									</div>
									<script type="text/javascript">
										function onTabChanged(tab){
											if(tab=="lyrics"){
												$("#lyricsbutton").addClass("current"); 
												$("#commentsbutton").removeClass("current"); 
												$("#comments-box").hide(); 
												$("#lyrics-box").show(); 
												$("#contributorsbutton").removeClass("current"); 
												$("#contributors-box").hide();
											}else if(tab=="comments"){
												$("#lyricsbutton").removeClass("current"); 
												$("#commentsbutton").addClass("current"); 
												$("#comments-box").show(); 
												$("#lyrics-box").hide();
												$("#contributorsbutton").removeClass("current"); 
												$("#contributors-box").hide();
											} else if(tab=="contributors"){
												$("#lyricsbutton").removeClass("current"); 
												$("#commentsbutton").removeClass("current"); 
												$("#comments-box").hide(); 
												$("#lyrics-box").hide(); 
												$("#contributors-box").show(); 
												$("#contributorsbutton").addClass("current");
											}
										}
										var hashVal = '';
										$(document).ready(function() {
										    hashVal = window.location.hash.split("#")[1];
										    var hashValshort = '';
										    if (hashVal != undefined) { hashValshort = hashVal.substring(0, 7); }
										    if(hashValshort == 'comment') {
										        $("#comments-box").show();
										        $("#lyrics-box").hide();
										        $("#lyricsbutton").removeClass("current");
										        $("#contributors-box").hide();
										        $("#contributorsbutton").removeClass("current");										        
										        $("#commentsbutton").addClass("current");
										    }
										    else if (hashVal == 'contributors'){
											    $("#comments-box").hide();
										        $("#lyrics-box").hide();
										        $("#contributors-box").show();
										        $("#contributorsbutton").addClass("current");
										        $("#lyricsbutton").removeClass("current");
										        $("#commentsbutton").removeClass("current");									    
										    }
										    else {
										        $("#comments-box").hide();
										        $("#lyrics-box").show();
										        $("#lyricsbutton").addClass("current");
										        $("#commentsbutton").removeClass("current");
										        $("#contributors-box").hide();
										        $("#contributorsbutton").removeClass("current");
										    }
										});
									</script>
								<div id="contributors-box" style="display:none;">
									<div id="contrib">
									<?php 
										$custom_field_keys = get_post_custom_keys();
                   foreach ( $custom_field_keys as $key => $customName ) {
                     $valuet = trim($customName);
                     if ( '_' == $valuet{0} )
                       continue;   
                     $mykey_values = get_post_custom_values($customName);
                     foreach ( $mykey_values as $key => $customValue ) {
                      if ($customName == "youtube" || $customName == "video-description")
                        continue;
										  echo "<p><strong>".$customName.": </strong>".$customValue."</p>";
                     }
                   }
									?>
									</div>
									<div id="gtizzy">
										<img src="/images/rg-videos-tortoise.jpg" title="fig. b - Galapagos Tortoise (a.k.a. G. Tizzy)" alt="fig. b - Galapagos Tortoise (a.k.a. G. Tizzy)">
									</div>
								</div>
								
								</div><!-- #post-## -->
				
				<?php endwhile; // end of the loop. ?>

			</div><!-- #content -->
		</div><!-- #container -->

<?php get_footer(); ?>