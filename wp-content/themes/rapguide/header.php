<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<title><?php
/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="https://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link href='https://fonts.googleapis.com/css?family=Permanent+Marker' rel='stylesheet' type='text/css' />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<script type="text/javascript" src="<?php bloginfo( 'template_url' ); ?>/jquery.scrollTo-1.4.2-min.js"></script>

<?php  // YOUTUBE FUNCTIONS 
// function to parse a youtube video for information about the vid  
function parseVideoEntry($entry) {  
$obj= new stdClass;  
  
// get nodes in media: namespace for media information  
$media = $entry->children('https://search.yahoo.com/mrss/');  
$obj->title = $media->group->title;  
$obj->description = $media->group->description;  
  
// get video player URL  
$attrs = $media->group->player->attributes();  
$obj->watchURL = $attrs['url'];  
  
// get video thumbnail  
$attrs = $media->group->thumbnail[0]->attributes();  
$obj->thumbnailURL = $attrs['url'];  
  
// get <yt:duration> node for video length  
$yt = $media->children('https://gdata.youtube.com/schemas/2007');  
$attrs = $yt->duration->attributes();  
$obj->length = $attrs['seconds'];  
  
// get <yt:stats> node for viewer statistics  
$yt = $entry->children('https://gdata.youtube.com/schemas/2007');  
$attrs = $yt->statistics->attributes();  
$obj->viewCount = $attrs['viewCount'];  
  
// get <gd:rating> node for video ratings  
$gd = $entry->children('https://schemas.google.com/g/2005');  
if ($gd->rating) {  
$attrs = $gd->rating->attributes();  
$obj->rating = $attrs['average'];  
} else {  
$obj->rating = 0;  
}  
  
// get <gd:comments> node for video comments  
$gd = $entry->children('https://schemas.google.com/g/2005');  
if ($gd->comments->feedLink) {  
$attrs = $gd->comments->feedLink->attributes();  
$obj->commentsURL = $attrs['href'];  
$obj->commentsCount = $attrs['countHint'];  
}  
  
//Get the author  
$obj->author = $entry->author->name;  
$obj->authorURL = $entry->author->uri;  
  
// get feed URL for video responses  
$entry->registerXPathNamespace('feed', 'https://www.w3.org/2005/Atom');  
$nodeset = $entry->xpath("feed:link[@rel='https://gdata.youtube.com/schemas/ 
2007#video.responses']"); 
if (count($nodeset) > 0) { 
$obj->responsesURL = $nodeset[0]['href']; 
} 
 
// get feed URL for related videos 
$entry->registerXPathNamespace('feed', 'https://www.w3.org/2005/Atom'); 
$nodeset = $entry->xpath("feed:link[@rel='https://gdata.youtube.com/schemas/  
2007#video.related']");  
if (count($nodeset) > 0) {  
$obj->relatedURL = $nodeset[0]['href'];  
}  
  
// return object to caller  
return $obj;  
} ?>

<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

</head>

<body <?php body_class(); ?>>
	<div id="outsidehead">
		<div id="headwrapper">
					<div id="access" role="navigation">
					  <?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
						<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a></div>
						<?php /* Our navigation menu.  If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.  */ ?>
						<div class="outer">
						<span class="inner <?php if (is_home()) echo 'current_page_item'; ?>"><a title="About" href="/">Home</a></span>
						<span class="inner <?php if (is_page('About')) echo 'current_page_item'; ?>"><a title="About" href="/about/">About</a></span>
						<span class="inner <?php if (is_page('Videos')) echo 'current_page_item'; ?>"><a title="Videos" href="/videos/">Videos</a></span>
						<span class="inner <?php if (is_page('Resources')) echo 'current_page_item'; ?>"><a title="Resources" href="/resources/">Resources</a></span>
						<span class="inner <?php if (is_page('Live Feed')) echo 'current_page_item'; ?>"><a title="News Feed" href="/news-feed/">News Feed</a></span>
						<span class="inner <?php if (is_page('Discussion')) echo 'current_page_item'; ?>"><a title="Discussion" href="/discussion/">Discussion</a></span>
						<span class="inner <?php if (is_page('Guides')) echo 'current_page_item'; ?>"><a title="Guides" href="/guides/">Guides</a></span>
						<span class="finish"></span>
						</div>
						
					</div><!-- #access -->
		</div>				
	</div>
	<div id="branding-container">
		<div id="branding" role="banner">
			<div id="socialcontainer"><div id="social"><span class="social">Follow Us: <a href="https://www.facebook.com/bababrinkman" target="_blank">Facebook</a> <span class="socialdiv">|</span> <a href="https://twitter.com/rapguide" target="_blank">Twitter</a> <span class="socialdiv">|</span> <a href="https://www.youtube.com/user/babasword" target="_blank">Youtube</a></span></div></div>
		</div><!-- #branding -->
	</div>
<div id="wrapper" class="hfeed">

	<div id="main">
