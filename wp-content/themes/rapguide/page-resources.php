<?php
/**
 * Template Name: Resources Page
 *
 * A custom page template with sidebars.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
						<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
			<div id="content" role="main">
			
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h1 class="two-column-title-left">Resources by Subject</h1>
				<?php the_content(); ?>
				<?php endwhile; else: ?>
				<?php endif; ?> 

				<div id="resources">
				<?php	  
					$args = array('categories' => '104,109');
					$tags = get_category_tags($args);
					foreach ($tags as $key => $tag) {
						?> 
						<img src="/images/idle.gif" class="plus" onclick="$('#postlink-<?php echo $key; ?>').slideToggle(); $('#videolink-<?php echo $key; ?>').slideToggle(); $('#blog-<?php echo $key; ?>').slideToggle(); $('#video-<?php echo $key; ?>').slideToggle(); $(this).toggle(); $(this).next().toggle(); $(this).next().next().toggleClass('active');">
						<img src="/images/active.gif" class="minus" onclick="$('#postlink-<?php echo $key; ?>').slideToggle(); $('#videolink-<?php echo $key; ?>').slideToggle(); $('#blog-<?php echo $key; ?>').slideToggle(); $('#video-<?php echo $key; ?>').slideToggle(); $(this).toggle(); $(this).prev().toggle(); $(this).next().toggleClass('active');" style="display:none;">
						<h2 onclick="$('#postlink-<?php echo $key; ?>').slideToggle(); $('#videolink-<?php echo $key; ?>').slideToggle(); $('#blog-<?php echo $key; ?>').slideToggle(); $('#video-<?php echo $key; ?>').slideToggle(); $(this).prev().prev().toggle(); $(this).prev().toggle(); $(this).toggleClass('active');">
						<?php echo $tag->tag_name ?></h2>
						
						<ul> <?php // show all posts in videos
						$tagqueryvid = new WP_Query('tag_id='.$tag->tag_id.'&posts_per_page=0&cat=104'); 
						if ($tagqueryvid->have_posts()) {?>
							<div id="blog-<?php echo $key ?>" style="display:none;">
							<img src="/images/idle.gif" class="plus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).next().toggle(); $(this).next().next().toggleClass('active');">
							<img src="/images/active.gif" class="minus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).prev().toggle(); $(this).next().toggleClass('active');" style="display:none;">
							<h3 onclick="$(this).parent().next().children().slideToggle(); $(this).prev().prev().toggle(); $(this).prev().toggle(); $(this).toggleClass('active');">Videos</h3></div>
							<ul id="videolink-<?php echo $key; ?>" style="display:none;">
							<?php 
							if ( $tagqueryvid->have_posts() ) : while ( $tagqueryvid->have_posts()) : $tagqueryvid->the_post(); 
									echo '<li class="postlink" style="display: none;">"';
									echo '<a href="'.get_permalink().'">'.get_the_title().'</a>';
									echo '"</li>';
							endwhile; else:
							endif;
							echo "</ul>";
						}

						// show all posts in blog
						$tagqueryblog = new WP_Query('tag_id='.$tag->tag_id.'&posts_per_page=0&cat=109'); 
						if ($tagqueryblog->have_posts()) {?>
							<div id="video-<?php echo $key; ?>" style="display:none;">
							<img src="/images/idle.gif" class="plus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).next().toggle(); $(this).next().next().toggleClass('active');">
							<img src="/images/active.gif" class="minus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).prev().toggle(); $(this).next().toggleClass('active');" style="display:none;">
							<h3 onclick="$(this).parent().next().children().slideToggle(); $(this).prev().prev().toggle(); $(this).prev().toggle(); $(this).toggleClass('active');">Blog Posts</h3></div>
							<ul id="postlink-<?php echo $key; ?>" style="display:none;">
							<?php 
							if ( $tagqueryblog->have_posts() ) : while ( $tagqueryblog->have_posts()) : $tagqueryblog->the_post(); 
									echo '<li class="postlink" style="display: none;">';
									echo '<a href="'.get_permalink().'">'.get_the_title().'</a>';
									echo '</li>';
							endwhile; else:
							endif;
							echo "</ul>";
						}
						echo "</ul>";
					}		
				?>
				</div> <!-- done with resources-->
				

				<div style="clear:both"></div>
								
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>


