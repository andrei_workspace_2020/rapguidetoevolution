<?php
/**
 * Template Name: News Feed Page
 *
 * A custom page template with sidebars.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
						<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
				
			<div id="content" role="main">

					<h1 class="two-column-title-left">News Feed</h1>
					<p><?php $feed = get_post_custom_values('news-feed-description',710);
					if ($feed != NULL){  foreach ( $feed as $key => $value ) { echo $value; }; }?></p>

<?php
$blogquery = new WP_Query('posts_per_page=10&cat=109'); 				
while ( $blogquery->have_posts() ) : $blogquery->the_post(); ?>

	<?php if (in_category('109')){?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="entry-meta">
				<?php twentyten_posted_on(); ?>
			</div><!-- .entry-meta -->
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
	<?php if ( is_archive() || is_search() ) : // Only display excerpts for archives and search. ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
	<?php else : ?>
			<div class="entry-content">
				<?php the_content( __( 'Read More... <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
			</div><!-- .entry-content -->
	<?php endif; ?>
	

			<div class="entry-utility">
				<span><a href="<?php echo get_permalink()?>#comments"><?php comments_number('No Comments Yet','1 Comment','% Comments'); ?>.</a></span>
							<span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyten' ), __( '1 Comment', 'twentyten' ), __( '% Comments', 'twentyten' ) ); ?></span> <br />
				<?php if ( count( get_the_category() ) ) : ?>
				<?php endif; ?>
				<?php
					$tags_list = get_the_tag_list( '', ', ' );
					if ( $tags_list ):
				?>
					<span class="tag-links">
						<?php printf( __( '<span class="%1$s">Keywords: </span> %2$s', 'twentyten' ), 'entry-utility-prep entry-utility-prep-tag-links', $tags_list ); ?>
					</span>
				<?php endif; ?>

				<?php edit_post_link( __( 'Edit', 'twentyten' ), '<span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
			</div><!-- .entry-utility -->
		</div><!-- #post-## -->
	<?php } // end category filter for blog posts only?>


<?php endwhile; // End the loop. Whew. ?>

<?php /* Display navigation to next/previous pages when applicable */ ?>
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
				<div id="nav-below" class="navigation">
					<div class="nav-previous"><?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'twentyten' ) ); ?></div>
					<div class="nav-next"><?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div>
				</div><!-- #nav-below -->
<?php endif; ?>

				<div style="clear:both"></div>
								
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>


