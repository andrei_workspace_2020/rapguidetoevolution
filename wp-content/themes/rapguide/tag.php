<?php
/**
 * The template for displaying Tag Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>

		<div id="container">
						<form action="/" id="searchform" method="get" role="search">
					<div>
						<label for="s" class="screen-reader-text">Search for:</label>
						<input type="text" id="s" name="s" value="">
						<input type="submit" value="Search" id="searchsubmit">
					</div>
				</form>
			<div id="content" role="main">

				<h1 class="two-column-title-left">Resources by Subject</h1>
					<p><?php $feed = get_post_custom_values('resources-description',708);
					if ($feed != NULL){  foreach ( $feed as $key => $value ) { echo $value; }; }?></p>
					
				<div id="resources">
				<?php	  
					$args = array('categories' => '104,109');
					$tags = get_category_tags($args);
					foreach ($tags as $key => $tag) {
						?> 
						<?php // if this is the right tag, we want to expand the tag out onload
						if (single_tag_title( '', false ) == ($tag->tag_name)) { ?>
							<script type="text/javascript">
								$(document).ready(function() {
									$('#postlink-<?php echo $key; ?>').slideToggle(); 
									$('#videolink-<?php echo $key; ?>').slideToggle(); 
									$('#blog-<?php echo $key; ?>').slideToggle(); 
									$('#video-<?php echo $key; ?>').slideToggle(); 
									$('#plus-<?php echo $key; ?>').toggle(); 
									$('#minus-<?php echo $key; ?>').toggle(); 
									$('#tagtitle-<?php echo $key; ?>').toggleClass('active');
									$.scrollTo('#tagtitle-<?php echo $key; ?>');
									
								});
							</script>
						<?php } ?>
						<img src="/images/idle.gif" id="plus-<?php echo $key ?>" class="plus" onclick="$('#postlink-<?php echo $key; ?>').slideToggle(); $('#videolink-<?php echo $key; ?>').slideToggle(); $('#blog-<?php echo $key; ?>').slideToggle(); $('#video-<?php echo $key; ?>').slideToggle(); $(this).toggle(); $(this).next().toggle(); $(this).next().next().toggleClass('active');">
						<img src="/images/active.gif" id="minus-<?php echo $key ?>" class="minus" onclick="$('#postlink-<?php echo $key; ?>').slideToggle(); $('#videolink-<?php echo $key; ?>').slideToggle(); $('#blog-<?php echo $key; ?>').slideToggle(); $('#video-<?php echo $key; ?>').slideToggle(); $(this).toggle(); $(this).prev().toggle(); $(this).next().toggleClass('active');" style="display:none;">
						<h2 id="tagtitle-<?php echo $key ?>" onclick="$('#postlink-<?php echo $key; ?>').slideToggle(); $('#videolink-<?php echo $key; ?>').slideToggle(); $('#blog-<?php echo $key; ?>').slideToggle(); $('#video-<?php echo $key; ?>').slideToggle(); $(this).prev().prev().toggle(); $(this).prev().toggle(); $(this).toggleClass('active');">
						<?php echo $tag->tag_name ?></h2>
						
						<ul> <?php // show all posts in videos
						$tagqueryvid = new WP_Query('tag_id='.$tag->tag_id.'&post_count=0&cat=104'); 
						if ($tagqueryvid->have_posts()) {?>
							<div id="blog-<?php echo $key ?>" style="display:none;">
							<img src="/images/idle.gif" class="plus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).next().toggle(); $(this).next().next().toggleClass('active');">
							<img src="/images/active.gif" class="minus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).prev().toggle(); $(this).next().toggleClass('active');" style="display:none;">
							<h3 onclick="$(this).parent().next().children().slideToggle(); $(this).prev().prev().toggle(); $(this).prev().toggle(); $(this).toggleClass('active');">Videos</h3></div>
							<ul id="videolink-<?php echo $key; ?>" style="display:none;">
							<?php 
							if ( $tagqueryvid->have_posts() ) : while ( $tagqueryvid->have_posts()) : $tagqueryvid->the_post(); 
									echo '<li class="postlink" style="display: none;">"';
									echo '<a href="'.get_permalink().'">'.get_the_title().'</a>';
									echo '"</li>';
							endwhile; else:
							endif;
							echo "</ul>";
						}

						// show all posts in blog
						$tagqueryblog = new WP_Query('tag_id='.$tag->tag_id.'&post_count=0&cat=109'); 
						if ($tagqueryblog->have_posts()) {?>
							<div id="video-<?php echo $key; ?>" style="display:none;">
							<img src="/images/idle.gif" class="plus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).next().toggle(); $(this).next().next().toggleClass('active');">
							<img src="/images/active.gif" class="minus" onclick="$(this).parent().next().children().slideToggle(); $(this).toggle(); $(this).prev().toggle(); $(this).next().toggleClass('active');" style="display:none;">
							<h3 onclick="$(this).parent().next().children().slideToggle(); $(this).prev().prev().toggle(); $(this).prev().toggle(); $(this).toggleClass('active');">Blog Posts</h3></div>
							<ul id="postlink-<?php echo $key; ?>" style="display:none;">
							<?php 
							if ( $tagqueryblog->have_posts() ) : while ( $tagqueryblog->have_posts()) : $tagqueryblog->the_post(); 
									echo '<li class="postlink" style="display: none;">';
									echo '<a href="'.get_permalink().'">'.get_the_title().'</a>';
									echo '</li>';
							endwhile; else:
							endif;
							echo "</ul>";
						}
						echo "</ul>";
					}		
				?>
				</div> <!-- done with resources-->
			</div><!-- #content -->
		</div><!-- #container -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
